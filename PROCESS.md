# other repo content

there is a file called [tool-versions](./.tool-versions) in this repo. This is used by the tool `asdf` to manage the versions of the tools I use. I have added the versions of the tools I used for this project to this file. It becomes especially helpful, w.r.t. teraform, when there are many workspaces having different tool versions.
Having a file like this in repositories helps keep unintended version changes from happening and, even more so, possible state drift from occuring.

# git

I needed to create a new SSH key and pair it with my gitlab account. I haven't used gitlab in several years and all the keys there were expired.

> NOTE
> here is the link to the [repo](https://gitlab.com/matthewaerose/infrastructure-applications)

# infrastructure

## terraform

The terraform I need to create concerns standing up a new GKE cluster.
I was provided a module to help me with this but that seemed like a bit of overkill. I instead opted for a slimmed down example from the [terraform documentation](https://developer.hashicorp.com/terraform/tutorials/kubernetes/gke).

## cloud creds, `terraform apply`, kubectl creds

### cloud creds

I have never used GCP from the CLI. So I needed to learn how to set up my credentials. The same tutorial linked above also gave me a good starting point for this.
Ultimately, I just followed the steps [here](https://cloud.google.com/sdk/docs/install#deb).

After configuring my credentials, I needed to enable the k8s API in the GCP console.

### terraform apply

After that, I was able to run `terraform plan` and see that it was able to connect to GCP and see the resources that would be created.

I applied my terraform and created the resources. I was able to see the cluster in the GCP console.

### kubectl creds

Now that I have a cluster, I need to configure my kubectl to be able to connect to it. This required another plugin [gke-gcloud-auth-plugin](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl#apt_1)

This command gets and sets the appropriate credentials for kubectl. It uses the output from terraform to get the cluster name and region and it generates a kubeconfig file for kubectl to use.

```bash
gcloud container clusters get-credentials $(terraform output -raw kubernetes_cluster_name) --region $(terraform output -raw region)
```

running kubectl commands, I can see that I am connected to the cluster.

# applications

## podinfo

For the second part of the task, I needed to deploy the podinfo application to the cluster. I borrowed some of the deployment content from the [podinfo repo](https://github.com/stefanprodan/podinfo/tree/master/kustomize). I do realize I could have applied this kustomization directly from the repo but I didn't want to skip the step from the task.

To apply the deployment, and similarly for other files, I used the following command:

```bash
$ kubectl apply -f deployment.yaml
```

To make things easier, deploy all files with kustomize using the following command:

```bash
$ pwd
> infrastructure-applications/applications
$ kubectl apply -k .
```

## creating a service

to have the pods within the deployment and replica set be accessible, I needed to create a service. I wrote a very simple service manifest [here](./applications/service.yaml).

## ingress and accessing the service

To be able to share my work, we need a few things. First, a static IP address. This IP address is assigned to a load balancer, that points to an ingress, that routes traffic to the appropriate service.

### static IP address

A static IP address is necessary to have a consistent address to point to. This can be accomplished with the command

```bash
$ gcloud compute addresses create <NAME> --global
```

### ingress and load balancer

I created an nginx ingress [here](./applications/ingress.yaml)
The ingress controller is taken care of by GKE.
The load balancer is created for us by indicating the [appropriate annotation](https://cloud.google.com/kubernetes-engine/docs/how-to/load-balance-ingress) in the ingress manifest.

```yaml
# this tells GCP to create a public load balancer
kubernetes.io/ingress.class: "gce"
# this tells GCP to use the static IP address we created
kubernetes.io/ingress.global-static-ip-name: "<NAME>"
```

_*NOTE*_
if you wanted to achieve this just using port forwading, you could use the following command:

```bash
$ kubectl port-forward service/podinfo 9898:9898
```

For testing locally, this method is ideal as it is much cheaper and faster.
